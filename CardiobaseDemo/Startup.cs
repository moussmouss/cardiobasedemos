﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CardiobaseDemo.Startup))]
namespace CardiobaseDemo
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
