﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PatientRegistration.aspx.cs" Inherits="CardiobaseDemo.PatientRegistration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section id="main-content">
        <section id="wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <div class="col-md-4 col-md-offset-4">
                                <h1>Patient Registration</h1>
                            </div>
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-1">
                                    <div class="form-group">
                                        <asp:Label Text="Patient Name" runat="server"/>
                                        <asp:TextBox runat="server" Enabled="True" CssClass="form-control input-sm" placeholder="Student Name"/>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label Text="DOB" runat="server"/>
                                        <asp:TextBox runat="server" Enabled="True" TextMode="Date" CssClass="form-control input-sm" placeholder="DOB"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
